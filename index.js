let express = require('express')
let app = express()
let server = require('http').Server(app)
let io = require('socket.io')(server)
let path = require('path')

app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '/index.html'))
})
var i = 0
let data = {
  socks: {},
  _alpha: {},
  get alpha() {
    let _alpha = Object
      .values(this._alpha)
      .reduce((i, j) => i + j) / Object.keys(this._alpha).length
    return Number(_alpha.toFixed(2))
  }
}

let sendToAll = (topic, message, exclude = null) => Object
  .values(data.socks)
  .forEach(sock =>
    sock.key != exclude
      ? sock.emit(topic, message)
      : null)

io.on('connection', (socket) => {
  let key = socket.handshake.query.key
  socket.emit('users', Object.keys(data.socks))
  socket.key = key
  console.log(key)
  data.socks[key] = socket
  sendToAll('join', key, key)
  socket.on('disconnect', () =>
    (delete data._alpha[key]) &&
    (delete data.socks[key]) &&
    sendToAll('exit', key))
  socket.on('motion', (m) =>
    (data._alpha[key] = m.a = Number(m.a.toFixed(2))) &&
    sendToAll('motion', data.alpha) ||
    sendToAll('deg', [key, m.a], key))
})
process.argv.push(0)
let port = process.argv.filter(isFinite)[0] || 3000
server.listen(port)